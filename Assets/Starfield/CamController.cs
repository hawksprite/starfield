﻿using UnityEngine;
using System.Collections;

public class CamController : MonoBehaviour {

    public float speed = 60.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.W))
        {
            transform.position = transform.position + new Vector3(0, speed * Time.smoothDeltaTime, 0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position = transform.position + new Vector3(0, -1 * speed * Time.smoothDeltaTime, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position = transform.position + new Vector3(speed * Time.smoothDeltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position = transform.position + new Vector3(-1 * speed * Time.smoothDeltaTime, 0, 0);
        }
    }
}
